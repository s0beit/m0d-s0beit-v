#include "stdafx.h"

DWORD64 g_dwRegistrationTablePtr = 0;

NativeRegistration** GetRegistrationTable() {
	if (!g_dwRegistrationTablePtr) {
		g_dwRegistrationTablePtr = Pattern::Scan(g_MainModuleInfo, "76 61 49 8B 7A 40 48 8D 0D");

		if (!g_dwRegistrationTablePtr) {
			Log::Fatal("Unable to find Native Registration Table");
		}

		g_dwRegistrationTablePtr += 6;

		DWORD64 dwAddressOfRegistrationTable = g_dwRegistrationTablePtr + *(DWORD*)(g_dwRegistrationTablePtr + 3) + 7;

		if (!dwAddressOfRegistrationTable ||
			dwAddressOfRegistrationTable < (DWORD64)g_MainModuleInfo.lpBaseOfDll ||
			dwAddressOfRegistrationTable >(DWORD64) g_MainModuleInfo.lpBaseOfDll + g_MainModuleInfo.SizeOfImage) {
			Log::Fatal("Error reading Native Registration Table opcode (0x%I64X)", dwAddressOfRegistrationTable);
		}

		g_dwRegistrationTablePtr = dwAddressOfRegistrationTable;

		Log::Msg("g_dwRegistrationTablePtr = 0x%I64X", g_dwRegistrationTablePtr);
	}

	return (NativeRegistration**)g_dwRegistrationTablePtr;
}

NativeHandler GetNativeHandler(UINT64 hash) {
	NativeRegistration** registrationTable = GetRegistrationTable();

	if (!registrationTable)
		return nullptr;

	NativeRegistration* table = registrationTable[hash & 0xFF];

	for (; table; table = table->nextRegistration)
	{
		for (UINT32 i = 0; i < table->numEntries; i++)
		{
			if (hash == table->hashes[i])
			{
				return table->handlers[i];
			}
		}
	}

	return nullptr;
}

DWORD64 g_dwThreadCollectionPtr = 0;

rage::pgPtrCollection<GtaThread>* GetGtaThreadCollection() {
	if (!g_dwThreadCollectionPtr) {
		g_dwThreadCollectionPtr = Pattern::Scan(g_MainModuleInfo, "48 8B ? ? ? ? ? 8B CA 4C 8B 0C C8");

		if (!g_dwThreadCollectionPtr) {
			Log::Fatal("Unable to find GTA Thread Pool");
		}

		DWORD64 dwAddressOfThreadCollection = g_dwThreadCollectionPtr + *(DWORD*)(g_dwThreadCollectionPtr + 3) + 7;

		if (!dwAddressOfThreadCollection ||
			dwAddressOfThreadCollection < (DWORD64)g_MainModuleInfo.lpBaseOfDll ||
			dwAddressOfThreadCollection > (DWORD64) g_MainModuleInfo.lpBaseOfDll + g_MainModuleInfo.SizeOfImage) {
			Log::Fatal("Error reading GTA Thread Pool opcode (0x%I64X)", dwAddressOfThreadCollection);
		}

		g_dwThreadCollectionPtr = dwAddressOfThreadCollection;

		Log::Msg("g_dwThreadCollectionPtr = 0x%I64X", g_dwThreadCollectionPtr);
	}

	return (rage::pgPtrCollection<GtaThread>*) g_dwThreadCollectionPtr;
}

GtaThread_VTable gGtaThreadOriginal;
GtaThread_VTable gGtaThreadNew;

eThreadState new_Run(GtaThread* This) {
	eThreadState state = gGtaThreadOriginal.Run(This);

	// mfw it actually works
	if (This->GetContext()->ScriptHash == 0x41D6F794) {
		NativeInvoke ni;

		Player player = ni.Invoke<0xAF65E5A58BE87D95, Player>();
		Ped playerPed = ni.Invoke<0x507DA4994C3D8EBD, Ped>();
		BOOL bPlayerExists = ni.Invoke<0xFD68187442384158, BOOL, Entity>(playerPed); //DOES_ENTITY_EXIST

		if (bPlayerExists) {
			ni.Invoke<0x60D71C675384BFB1, Void, Any, BOOL>(player, TRUE); //SET_PLAYER_INVINCIBLE
		}
	}

	return state;
}

void AttemptScriptHook() {
	rage::pgPtrCollection<GtaThread>* threadCollection = GetGtaThreadCollection();

	if (!threadCollection) {
		return;
	}

//	Log::Msg("Thread Collection: 0x%I64X", threadCollection);
//	Log::Msg("Thread Count: %d", threadCollection->count());

	for (UINT16 i = 0; i < threadCollection->count(); i++) {
		GtaThread* pThread = threadCollection->at(i);

		if (!pThread)
			continue;

		// We're going to start trying to target threads
		// This script ID is just one i found that was called fairly regularly
		// it may not be ideal for some things (like a full blown menu)
		// Feel free to experiment
		if (pThread->GetContext()->ScriptHash != 0x41D6F794) {
			continue;
		}

		// Now what? We need to find a target thread and hook it's "Tick" function
		if (gGtaThreadOriginal.Deconstructor == NULL) {
			memcpy(&gGtaThreadOriginal, (DWORD64*)((DWORD64*)pThread)[0], sizeof(gGtaThreadOriginal));
			memcpy(&gGtaThreadNew, &gGtaThreadOriginal, sizeof(GtaThread_VTable));

			gGtaThreadNew.Run = new_Run;
		}
		
		if (((DWORD64*)pThread)[0] != (DWORD64)&gGtaThreadNew) {
			Log::Msg("Hooking thread [%i] (0x%X)", i, pThread->GetContext()->ScriptHash);
			((DWORD64*)pThread)[0] = (DWORD64)&gGtaThreadNew;
			Log::Msg("Hooked thread [%i] (0x%X)", i, pThread->GetContext()->ScriptHash);
		}
	}
}

DWORD WINAPI lpHookScript(LPVOID lpParam) {
	while (true) {
		AttemptScriptHook();

		Sleep(100);
	}

	return 0;
}

void SpawnScriptHook() {
	CreateThread(0, 0, lpHookScript, 0, 0, 0);
}