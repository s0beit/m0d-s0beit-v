// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

// Windows Header Files:
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <intrin.h>
#include <string>
#include <vector>
#include <sstream>
#include <Psapi.h>

#pragma comment(lib, "Psapi")

//
extern MODULEINFO g_MainModuleInfo;

// DX11 Header Files:
#include <d3d11.h>
#include <d3d11_1.h>

#pragma comment(lib, "d3d11.lib")

// Mine
#include "Iat.h"
#include "Log.h"
#include "Pattern.h"

// Rockstar
#include "Types.h"
#include "pgCollection.h"
#include "scrThread.h"

// Main
#include "dx.h"
#include "script.h"