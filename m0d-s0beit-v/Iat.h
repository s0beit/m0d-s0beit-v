#pragma once

// This doesn't belong to me, it's so clean I figured why reinvent the wheel?
// Source: http://jpassing.com/2008/01/06/using-import-address-table-hooking-for-testing/

extern HRESULT PatchIat(__in HMODULE Module, __in PSTR ImportedModuleName, __in PSTR ImportedProcName, __in PVOID AlternateProc, __out_opt PVOID *OldProc);
extern HRESULT PatchIat(__in HMODULE Module, __in PSTR ImportedModuleName, __in DWORD Ordinal, __in PVOID AlternateProc, __out_opt PVOID *OldProc);