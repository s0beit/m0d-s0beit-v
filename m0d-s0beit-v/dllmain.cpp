#include "stdafx.h"

MODULEINFO g_MainModuleInfo = { 0 };

DWORD WINAPI lpDirectXHook(LPVOID lpParam)
{
	while (DXHook() == false)
		Sleep(100);

	return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
		Log::Init(hModule);
		Log::Msg("m0d-s0beit-v loaded");

		if (!GetModuleInformation(GetCurrentProcess(), GetModuleHandle(0), &g_MainModuleInfo, sizeof(g_MainModuleInfo))) {
			Log::Fatal("Unable to get MODULEINFO from GTA5.exe");
		}

		Log::Msg("GTA5 [0x%I64X][0x%X]", g_MainModuleInfo.lpBaseOfDll, g_MainModuleInfo.SizeOfImage);

		SpawnScriptHook();

		// We might not even need DX11 hooks since the script system allows rendering
		//CreateThread(0, 0, lpDirectXHook, 0, 0, 0);
	}

	return TRUE;
}

