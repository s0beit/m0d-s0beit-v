#include "stdafx.h"

char g_baseLogPath[MAX_PATH];
char g_logBuffer[409600];

void Log::Init(HMODULE hModule) {
	memset(g_baseLogPath, 0, sizeof(g_baseLogPath));

	if (GetModuleFileNameA(hModule, g_baseLogPath, MAX_PATH) != 0) {
		size_t slash = -1;

		for (size_t i = 0; i < strlen(g_baseLogPath); i++) {
			if (g_baseLogPath[i] == '/' || g_baseLogPath[i] == '\\') {
				slash = i;
			}
		}

		if (slash != -1) {
			g_baseLogPath[slash + 1] = '\0';
		}
		else {
			// How??
			MessageBoxA(NULL, "Unable to parse target module path", "ERROR", MB_OK);
			ExitProcess(0);
		}
	}
	else {
		// How??
		MessageBoxA(NULL, "GetModuleFileNameA failed", "ERROR", MB_OK);
		ExitProcess(0);
	}
}

void WriteLog(const char* section, const char* data) {
	char path[MAX_PATH];
	strcpy_s(path, g_baseLogPath);
	strcat_s(path, "hook.log");

	FILE* fp = NULL;

	errno_t e = fopen_s(&fp, path, "a");

	if (e == 0 && fp) {
		fprintf_s(fp, "[%s] %s\n", section, data);
		fclose(fp);
	}
}

void Log::Debug(const char* fmt, ...) {
#ifdef __DEBUG
	va_list va_alist;

	char logBuffer[2048];

	ZeroMemory(logBuffer, sizeof(logBuffer));

	va_start(va_alist, fmt);
	_vsnprintf_s(logBuffer, sizeof(logBuffer), fmt, va_alist);
	va_end(va_alist);

	WriteLog("DEBUG", logBuffer);
#endif
}

void Log::Msg(const char* fmt, ...) {
	va_list va_alist;

	ZeroMemory(g_logBuffer, sizeof(g_logBuffer));

	va_start(va_alist, fmt);
	_vsnprintf_s(g_logBuffer, sizeof(g_logBuffer), fmt, va_alist);
	va_end(va_alist);

	WriteLog("MSG", g_logBuffer);
}

void Log::Fatal(const char* fmt, ...) {
	va_list va_alist;

	ZeroMemory(g_logBuffer, sizeof(g_logBuffer));

	va_start(va_alist, fmt);
	_vsnprintf_s(g_logBuffer, sizeof(g_logBuffer), fmt, va_alist);
	va_end(va_alist);

	WriteLog("FATAL", g_logBuffer);

	MessageBoxA(NULL, g_logBuffer, "FATAL ERROR", MB_ICONERROR);

	ExitProcess(0);
}