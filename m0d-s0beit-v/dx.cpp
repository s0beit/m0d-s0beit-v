#include "stdafx.h"

typedef HRESULT(__stdcall *D3D11Present_t) (IDXGISwapChain* This, UINT SyncInterval, UINT Flags);

D3D11Present_t pD3D11Present = NULL;

HRESULT __stdcall new_D3D11Present(IDXGISwapChain* This, UINT SyncInterval, UINT Flags) {
	//

	return pD3D11Present(This, SyncInterval, Flags);
}

bool HookPresentFunction(IDXGISwapChain* swapChain)
{
	MEMORY_BASIC_INFORMATION64 mbi;

	DWORD_PTR* vt = *(DWORD_PTR**)swapChain;

	if (!VirtualQuery(vt, reinterpret_cast<PMEMORY_BASIC_INFORMATION>(&mbi), sizeof(mbi)))
		return false;

	if (!VirtualProtect((LPVOID) mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &mbi.Protect))
		return false;

	pD3D11Present = (D3D11Present_t)vt[8];

	vt[8] = (DWORD_PTR)new_D3D11Present;

	if (!VirtualProtect((LPVOID)mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect))
		return false;

	return true;
}

bool DXHook()
{
	HMODULE hD3D11 = GetModuleHandleA("d3d11.dll");

	if (hD3D11 == NULL)
		return false;

	// Create Our Device
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	HWND hWnd = GetForegroundWindow();

	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hWnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.Windowed = (GetWindowLong(hWnd, GWL_STYLE) & WS_POPUP) != 0 ? FALSE : TRUE;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	IDXGISwapChain* pTempSwapChain = nullptr;
	ID3D11Device* pTempDevice = nullptr;
	ID3D11DeviceContext* pTempContext = nullptr;

	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, &featureLevel, 1
		, D3D11_SDK_VERSION, &swapChainDesc, &pTempSwapChain, &pTempDevice, NULL, &pTempContext)))
	{
		Log::Msg("Failed to CreateDeviceAndSwapChain");

		return false;
	}

	DWORD_PTR dwVTableAddress = NULL;

	DWORD_PTR* pSwapChainVtable = nullptr;

	pSwapChainVtable = (DWORD_PTR*)pTempSwapChain;
	pSwapChainVtable = (DWORD_PTR*)pSwapChainVtable[0];

	MEMORY_BASIC_INFORMATION64 mbi = { 0 };

	IDXGISwapChain* pRealSwapChain = nullptr;

	for (DWORD_PTR memptr = 0x10000; memptr < 0x7FFFFFFEFFFF; memptr = mbi.BaseAddress + mbi.RegionSize) {
		if (!VirtualQuery(reinterpret_cast<LPCVOID>(memptr), reinterpret_cast<PMEMORY_BASIC_INFORMATION>(&mbi), sizeof(MEMORY_BASIC_INFORMATION))) {
			continue;
		}

		//Filter Regions
		if (mbi.State != MEM_COMMIT || mbi.Protect == PAGE_NOACCESS || mbi.Protect & PAGE_GUARD) {
			continue;
		}

		DWORD_PTR len = mbi.BaseAddress + mbi.RegionSize;

		for (DWORD_PTR current = mbi.BaseAddress; current < len; ++current) {
			__try {
				dwVTableAddress = *(DWORD_PTR*)current;
			}
			__except (1) {
				continue;
			}

			if (dwVTableAddress == (DWORD_PTR)pSwapChainVtable) {
				if (current == (DWORD_PTR)pTempSwapChain)
					continue; // Avoid using the pointer we created ourselves

				pRealSwapChain = (IDXGISwapChain*)current;

				break;
			}
		}

		if (pRealSwapChain)
			break; // Exit this loop as well
	}

	Log::Msg("Real SwapChain: 0x%I64X", pRealSwapChain);

	// Release our created stuff
	if (pTempContext != nullptr) {
		pTempContext->Release();
		pTempContext = nullptr;
	}

	if (pTempDevice != nullptr) {
		pTempDevice->Release();
		pTempDevice = nullptr;
	}

	if (pTempSwapChain != nullptr) {
		pTempSwapChain->Release();
		pTempSwapChain = nullptr;
	}

	// Now we can hook :-)
	return HookPresentFunction(pRealSwapChain);
}